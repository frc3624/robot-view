# 2018 Robot AR App
During competitions, the robot isn't always readily availible to showcase to judges, so I made this program so people can see the basic design of the robot and if implemented, showcase some of its abilities
## How to install
Right now, git isnt complying with me, so you will have to open the project folder in Unity and build
## How to use
In the 'AR targets' folder, there are 5 QR codes. They should be put on the floor in the layout shown below.
![1###\n2##3##\n4###5](layout.png)
You dont have to use *all* of them, but you can use the largest space possible if you do.