﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnChooser : MonoBehaviour
{

    public static string robot = null;

    public GameObject powerUpPrefab;
    public GameObject deepSpacePrefab;

    /// <summary>
    /// Spawns the robot specified in the variable robot, does nothing otherwise
    /// </summary>
    void Start()
    {
        if (robot != null)
        {
            GameObject newRobot;
            if (robot.Equals("Power Up"))
                newRobot = Instantiate(powerUpPrefab) as GameObject;
            else if (robot.Equals("Deep Space"))
                newRobot = Instantiate(deepSpacePrefab) as GameObject;
        }
    }

}
