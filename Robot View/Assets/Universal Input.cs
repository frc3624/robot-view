﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class UniversalInput
{
    public static bool useScreenControls = false;
    public static float GetAxis(string axis)
    {
        return useScreenControls ? CrossPlatformInputManager.GetAxis(axis) : Input.GetAxis(axis);
    }
    public static bool GetButton(string button)
    {
        return useScreenControls ? CrossPlatformInputManager.GetButton(button) : Input.GetButton(button);
    }
    public static bool GetButtonDown(string button)
    {
        return useScreenControls ? CrossPlatformInputManager.GetButtonDown(button) : Input.GetButtonDown(button);
    }
}
